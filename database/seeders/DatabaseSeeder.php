<?php

namespace Database\Seeders;

use App\Imports\DashboardTableImport;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $file = 'Задание_dates.xlsx';
        (new DashboardTableImport)
            ->import('public/' . $file, 'local', \Maatwebsite\Excel\Excel::XLSX);
        // \App\Models\User::factory(10)->create();
    }
}
