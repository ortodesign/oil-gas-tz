<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExampleDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('example_dashboard', function (Blueprint $table) {
            //            $table->id();
            // dubious way (date and integer in same column), but one table needed
            $table->integer('id')->nullable();
            $table->string( 'company')->nullable();
            $table->integer('fact_qliq_data1')->nullable();
            $table->integer('fact_qliq_data2')->nullable();
            $table->integer('fact_qoil_data1')->nullable();
            $table->integer('fact_qoil_data2')->nullable();
            $table->integer('forecast_qliq_data1')->nullable();
            $table->integer('forecast_qliq_data2')->nullable();
            $table->integer('forecast_qoil_data1')->nullable();
            $table->integer('forecast_qoil_data2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
