<?php

namespace App\Http\Controllers;

use App\Imports\DashboardTableImport;
use App\Models\ExampleDashboard;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use Maatwebsite\Excel\HeadingRowImport;


class DashboardController extends Controller
{

    /** fill database table from raw xlsx
     * @param string $file
     * @return ExampleDashboard[]|\Illuminate\Database\Eloquent\Collection
     */
    public function importExcel($file = 'Задание_dates.xlsx')
    {
        (new DashboardTableImport)
            ->import('public/' . $file, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return ExampleDashboard::all();
    }


    /** (Not used)
     * get Headings (with dates on 3 row) from raw xlsx
     * @param string $file
     * @return array
     */
    public function getRawHeadings($file = 'Задание_dates.xlsx'): array
    {
        $headings = (new HeadingRowImport(3))->toArray('public/' . $file);
        return $headings;
    }


    /** convert 1st row with Excel date format to datetime and string formatted 'Y-m-d'
     * @return mixed
     */
    public function getDates()
    {
        $dates = collect(
            ExampleDashboard::query()
                ->take(1)
                ->get()
                ->first())
            ->map(function ($v, $k) {
                if (!in_array($k, ['id', 'company']))
                    return Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($v))->format('Y-m-d');
            });
        return $dates;
    }

    /** By task
     * (TODO generate response with data to front fields )
     * @return array
     */
    public function getTableDatesSummary()
    {
        $table = [];

        $dates = collect(ExampleDashboard::query()->first())
            ->filter(function ($item) {
                return $item;
            });
        $sums = $dates->map(function ($item) {
            $item = null;
            return $item;
        })->toArray();

        $builder = ExampleDashboard::query()->whereNotNull('id');

        //grouping sums by date
        $dates->each(function ($item, $k) use (&$table, $sums, $builder) {
            //convert date from excel date format (integer) to datetime and format as string
            $sums['date'] = Carbon::parse(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item))->format('Y-m-d');;
//            $sums['dateExel'] = $item;
            $sums[$k] = $builder->sum($k);
            $table[$item][] = $sums;
        });

        //merge values from groups to same date
        $t = [];
        foreach ($table as $key => &$sub) {
            if (sizeof($sub) > 1) {
                for ($i = 0; $i <= sizeof($sub) - 1; $i++) {
                    if ($i > 0) {
                        foreach ($sub[$i] as $k => &$v) {
                            if (is_null($sub[0][$k])) $sub[0][$k] = $v;
                        }
                    }
                }
            }
            $t[] = $sub[0];
        }

        return $t;
    }


    public function getData()
    {
        return ExampleDashboard::query()->whereNotNull('id')->get();
    }

    public function getSum()
    {
        $columns = Schema::getColumnListing('example_dashboard');

        $builderTable = ExampleDashboard::query()->whereNotNull('id');
        $results = [];
        foreach ($columns as $column) {
            $results[$column] = !in_array($column, ['id', 'company']) ? $builderTable->sum($column) : null;
        }
        return [$results];
    }


}
