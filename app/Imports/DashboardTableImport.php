<?php

namespace App\Imports;

use App\Models\ExampleDashboard;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\Importable;


class DashboardTableImport implements ToModel, WithStartRow//, WithHeadingRow
{
    use Importable;

    //skip heading start import at 3 row
    public function startRow(): int
    {
        return 3;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new ExampleDashboard([
            'id' => $row[0],
            'company' => $row[1],
            'fact_qliq_data1' => $row[2],
            'fact_qliq_data2' => $row[3],
            'fact_qoil_data1' => $row[4],
            'fact_qoil_data2' => $row[5],
            'forecast_qliq_data1' => $row[6],
            'forecast_qliq_data2' => $row[7],
            'forecast_qoil_data1' => $row[8],
            'forecast_qoil_data2' => $row[9],
        ]);
    }
}
