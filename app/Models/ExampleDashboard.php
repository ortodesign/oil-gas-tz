<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExampleDashboard
 * @package App\Models
 * @property boolean $timestamps //fix phpStorm bug
 */
class ExampleDashboard extends Model
{
    use HasFactory;
    protected $table = 'example_dashboard';
    protected $guarded = [];
    public $timestamps  = false;

    protected $casts = [
    ];
}
