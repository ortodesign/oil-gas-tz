<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//import from excel to BD (moved to DatabaseSeeder)
//Route::get('/importExcel/{file?}','App\Http\Controllers\DashboardController@importExcel');

Route::get('/getData','App\Http\Controllers\DashboardController@getData');
Route::get('/getDates','App\Http\Controllers\DashboardController@getDates');
Route::get('/getSum','App\Http\Controllers\DashboardController@getSum');

Route::get('/getTableDatesSummary','App\Http\Controllers\DashboardController@getTableDatesSummary');

Route::get('/{any}', 'App\Http\Controllers\PagesController@index')->where('any', '.*');
