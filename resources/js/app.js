require('./bootstrap');
import Vue from 'vue'
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue';
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

//Main pages
import App from './views/app.vue'


const app = new Vue({
    el: '#app',
    components: { App }
});
